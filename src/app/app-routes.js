export default function Routes($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home')

    const homeViewState = {
        name: 'homeView',
        url: 'home',
        component: 'homeView'
    }

    // const detailsViewState = {
    //     name: 'detailsView',
    //     url: 'details',
    //     component: 'detailsView'
    // }

    const checkoutViewState = {
        name: 'checkoutView',
        url: 'checkout',
        component: 'checkoutView'
    }

    // const finishOrderViewState = {
    //     name: 'finishOrderView',
    //     url: 'finishOrder',
    //     component: 'finishOrder'
    // }

    $stateProvider
        .state(homeViewState)
        // .state(detailsViewState)
        .state(checkoutViewState)
        // .state(finishOrderViewState)
}

Routes.inject = [ '$stateProvider', '$urlRouterProvider'];