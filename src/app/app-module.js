import angular from 'angular'
import uiRouter from 'angular-ui-router'
import ngRedux from 'ng-redux'

import Routes from './app-routes'
import AppController from './app-controller'
import HomeViewModule from './home-view/home-view-module'

const OWN_DEPS = [
    HomeViewModule.name
]

const EXTERNAL_DEPS = [
    ngRedux,
    uiRouter
]

const DEPS = [
    ...OWN_DEPS,
    ...EXTERNAL_DEPS
]

export default angular
    .module('E-CommerceApp', DEPS)
    .controller('AppController', AppController)
    .config(Routes)
    // .config(ReduxConfig)