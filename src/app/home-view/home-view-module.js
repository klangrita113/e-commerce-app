import homeViewComponent from './home-view.component'
import topNavigationComponent from './top-navigation/top-navigation.component'
import watchesListComponent from './watches-list/watches-list.component'

import leftNavigationComponent from './left-navigation/left-navigation.component'
import brandsComponent from './left-navigation/brands/brands.component'
import colorsComponent from './left-navigation/colors/colors.component'
import categoriesComponent from './left-navigation/categories/categories.component'




export default angular
    .module('HomeViewModule', [])
    .component('homeView', homeViewComponent )
    .component('topNavigation', topNavigationComponent)
    .component('leftNavigation', leftNavigationComponent)
    .component('watchesList', watchesListComponent)
    .component('brandsComponent', brandsComponent)
    .component('colorsComponent', colorsComponent)
    .component('categoriesComponent', categoriesComponent)